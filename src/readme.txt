=== Dynoshop ===

Requires at least: 4.5
Tested up to: 5.8.1
Requires PHP: 7.0
Stable tag: 1.0
License: GNU General Public License v2 or later
License URI: LICENSE

An all-in-one & drag and drop eCommerce Theme For WordPress.

== Description ==

An all-in-one & drag and drop eCommerce Theme For WordPress. Compatible with WooCommerce,Easy Digital Download and Elementor.



== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.



== Changelog ==

= 1.0 - November 01, 2021 =
* Initial release